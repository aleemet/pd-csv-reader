# CSV importer

> A Vue.js project for importing and reading CSV files. As part of Pipedrive's 2018 Industrial Master's program test assignment.

> All of requirements are implemented except upload progress indication. 

> By Alar Leemet.

## Building and Running Setup Locally or in Docker

``` bash

# install dependencies
npm install

# Build and start server locally
npm start 


# Build docker image for containerisation
docker build -t <docker id>/csv-importer .

# Run docker image for launching the application in a container (may take a while to respond after image is running!)
# It is important to keep the ports equal for the front-end to query the backend!
docker run -p 8080:8080 -d <docker id>/csv-importer

```

## Features

* Importing a CSV file. 
	* Can be imported with API endpoint "/import", content type has to be undefined and payload has to be a form where key is "uploadCsv" and value is the imported file.
* Replace imported CSV file with a new one.
* Detecting if the server already has a CSV file - import form displayed, results ('Employees') table displayed and search field disabled/enabled accordingly.
* Searching the CSV file by entry's 'name' column with autocomplete. Autocomplete is updated after 0.5 s since last keystroke. 
	* Can search with API endpoint "/search", content type has to be application/json and payload is a json in the form of { "query": "<Your search string>" } .
* After typing search string, hitting enter retrieves all items containing the search string. 
* After typing search string, selecting item from autocomplete list displays only that specific entry in the result table. Clicking or arrow keys can be used, highlighted item is chosen.

