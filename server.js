const express = require('express');
const bodyParser = require('body-parser');
const multer = require('multer');
const path = require('path');
const {PORT = 8080} = process.env;
const {HOST = '0.0.0.0'} = process.env;

const app = express();
app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.use(bodyParser.json());
app.use(express.static(path.resolve(__dirname, './dist')))


let upload = multer();
let parsedCSV = null;

const parseCSV = (text) => {
  // set this to whatever number of items you can process at once
  const allCSVRows = text.split(/\r\n|\n/);
  let table = [];
  let headers =  {
    id: "ID",
    name: "Name",
    age: "Age",
    address: "Address",
    team: "Team"
  };

  for (var i=0; i<allCSVRows.length-1; i++) {
      let name_address_team = allCSVRows[i].split('"');
      let fst = name_address_team[0].split(",")
        .filter((el) => el !== '');
      let snd = name_address_team[2].split(",")
        .filter((el) => el !== '');
      let splitRow = fst.concat(name_address_team[1])
        .concat(snd);
      if (splitRow.length === Object.keys(headers).length) {
        table.push({
          id: splitRow[0],
          name: splitRow[1],
          age: splitRow[2],
          address: splitRow[3],
          team: splitRow[4]
        });
      }
  }
  return table;
};

const filterResults = (query) => {
  return parsedCSV.filter((item) => {
    return item.name.toLowerCase().indexOf(query.toLowerCase()) > -1;
  });
};

app.get('/',(req, res) => {
  res.sendFile(path.resolve(__dirname, "./dist/index.html"))
});

app.get('/fileImported', (req, res) => {
  return res.json(parsedCSV !== null);
});

app.post('/import', upload.single('uploadCsv'), (req, res, next) => {
  const csvAsText = req.file.buffer.toString();
  if (!csvAsText) {
    return res.status(400).json({error: 'Request did not include a file!'})
  }
  parsedCSV = parseCSV(csvAsText);
  return res.status(200).json({msg: 'CSV imported!'})
});

app.post('/search', (req,res) => {
  const query = req.body.query;
  const results = filterResults(query);

  return res.status(200).json({results})
});

app.listen(PORT, HOST, () => {
  console.log(`Running on http://${HOST}:${PORT}`)
});
